#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail


CRATE_API=ibus
CRATE_SYS=ibus-sys

CONFIG_API=${CRATE_API}/Gir.toml
CONFIG_SYS=${CRATE_SYS}/Gir.toml

GIR=./gir/target/debug/gir

# Build the gir tool
git submodule update --init
(cd gir && cargo build)

# Regenerate the sys crate
rm -rf ${CRATE_SYS}/{build.rs,src,tests}
${GIR} -c ${CONFIG_SYS} -d gir-files -o ${CRATE_SYS}

# Regenerate the api crate
rm -rf ${CRATE_API}/src/auto
${GIR} -c ${CONFIG_API} -d gir-files -o ${CRATE_API}
