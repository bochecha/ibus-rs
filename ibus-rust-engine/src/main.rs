#[macro_use]
extern crate glib;

use glib::prelude::*;
use glib::subclass;
//use glib::subclass::object::{glib_object_impl, glib_object_subclass};
use glib::subclass::prelude::*;

use ibus;
use ibus::BusExt;

pub struct EngineDummy {
}

impl ObjectSubclass for EngineDummy {
    const NAME: &'static str = "EngineDummy";

    type ParentType = ibus::Engine;
    type Instance = subclass::simple::InstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib_object_subclass!();

    // Called right before the first time an instance of the new type is
    // created. Here class specific settings can be performed, including
    // installation of properties and registration of signals for the new type.
    fn class_init(klass: &mut subclass::simple::ClassStruct<Self>) {
        println!("EngineDummy.class_init()");
    }

    // Called every time a new instance is created. This should return
    // a new instance of our type with its basic values.
    fn new() -> Self {
        println!("EngineDummy.new()");
        Self {}
    }
}

impl ObjectImpl for EngineDummy {
    glib_object_impl!();

    // Called right after construction of the instance.
    fn constructed(&self, obj: &glib::Object) {
        // Chain up to the parent type's implementation of this virtual method.
        self.parent_constructed(obj);

        // And here we could do our own initialization.
        println!("EngineDummy.constructed()");
    }
}

fn main() {
    let mainloop = glib::MainLoop::new(None, false);
    let mainloop2 = mainloop.clone();

    let bus = ibus::Bus::new();
    bus.connect_disconnected(move |_| {
        mainloop2.quit();
    });

    let factory = ibus::Factory::new(bus.get_connection());
    factory.add_engine("EngineDummy", EngineDummy::get__type());

    bus.request_name("org.freedesktop.IBus.Cangjie", 0);

    mainloop.run();
}
