#[macro_use]
extern crate bitflags;
extern crate libc;

extern crate glib_sys as glib_ffi;
extern crate gobject_sys as gobject_ffi;
extern crate ibus_sys as ffi;

#[macro_use]
extern crate glib;

mod auto;

pub use auto::*;
