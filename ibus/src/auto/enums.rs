// This file was generated by gir (https://github.com/gtk-rs/gir @ 11adffa)
// from gir-files (https://github.com/gtk-rs/gir-files @ ???)
// DO NOT EDIT

use ffi;
use glib::StaticType;
use glib::Type;
use glib::translate::*;
use glib::value::FromValue;
use glib::value::FromValueOptional;
use glib::value::SetValue;
use glib::value::Value;
use gobject_ffi;
use std::fmt;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[derive(Clone, Copy)]
pub enum Orientation {
    Horizontal,
    Vertical,
    System,
    #[doc(hidden)]
    __Unknown(i32),
}

impl fmt::Display for Orientation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Orientation::{}", match *self {
            Orientation::Horizontal => "Horizontal",
            Orientation::Vertical => "Vertical",
            Orientation::System => "System",
            _ => "Unknown",
        })
    }
}

#[doc(hidden)]
impl ToGlib for Orientation {
    type GlibType = ffi::IBusOrientation;

    fn to_glib(&self) -> ffi::IBusOrientation {
        match *self {
            Orientation::Horizontal => ffi::IBUS_ORIENTATION_HORIZONTAL,
            Orientation::Vertical => ffi::IBUS_ORIENTATION_VERTICAL,
            Orientation::System => ffi::IBUS_ORIENTATION_SYSTEM,
            Orientation::__Unknown(value) => value
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::IBusOrientation> for Orientation {
    fn from_glib(value: ffi::IBusOrientation) -> Self {
        match value {
            0 => Orientation::Horizontal,
            1 => Orientation::Vertical,
            2 => Orientation::System,
            value => Orientation::__Unknown(value),
        }
    }
}

impl StaticType for Orientation {
    fn static_type() -> Type {
        unsafe { from_glib(ffi::ibus_orientation_get_type()) }
    }
}

impl<'a> FromValueOptional<'a> for Orientation {
    unsafe fn from_value_optional(value: &Value) -> Option<Self> {
        Some(FromValue::from_value(value))
    }
}

impl<'a> FromValue<'a> for Orientation {
    unsafe fn from_value(value: &Value) -> Self {
        from_glib(gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl SetValue for Orientation {
    unsafe fn set_value(value: &mut Value, this: &Self) {
        gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, this.to_glib())
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[derive(Clone, Copy)]
pub enum PropState {
    Unchecked,
    Checked,
    Inconsistent,
    #[doc(hidden)]
    __Unknown(i32),
}

impl fmt::Display for PropState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "PropState::{}", match *self {
            PropState::Unchecked => "Unchecked",
            PropState::Checked => "Checked",
            PropState::Inconsistent => "Inconsistent",
            _ => "Unknown",
        })
    }
}

#[doc(hidden)]
impl ToGlib for PropState {
    type GlibType = ffi::IBusPropState;

    fn to_glib(&self) -> ffi::IBusPropState {
        match *self {
            PropState::Unchecked => ffi::PROP_STATE_UNCHECKED,
            PropState::Checked => ffi::PROP_STATE_CHECKED,
            PropState::Inconsistent => ffi::PROP_STATE_INCONSISTENT,
            PropState::__Unknown(value) => value
        }
    }
}

#[doc(hidden)]
impl FromGlib<ffi::IBusPropState> for PropState {
    fn from_glib(value: ffi::IBusPropState) -> Self {
        match value {
            0 => PropState::Unchecked,
            1 => PropState::Checked,
            2 => PropState::Inconsistent,
            value => PropState::__Unknown(value),
        }
    }
}

impl StaticType for PropState {
    fn static_type() -> Type {
        unsafe { from_glib(ffi::ibus_prop_state_get_type()) }
    }
}

impl<'a> FromValueOptional<'a> for PropState {
    unsafe fn from_value_optional(value: &Value) -> Option<Self> {
        Some(FromValue::from_value(value))
    }
}

impl<'a> FromValue<'a> for PropState {
    unsafe fn from_value(value: &Value) -> Self {
        from_glib(gobject_ffi::g_value_get_enum(value.to_glib_none().0))
    }
}

impl SetValue for PropState {
    unsafe fn set_value(value: &mut Value, this: &Self) {
        gobject_ffi::g_value_set_enum(value.to_glib_none_mut().0, this.to_glib())
    }
}

