#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail


FDOSDK_INFO=($(flatpak info --show-location --show-commit org.freedesktop.Sdk//18.08))
FDOSDK_COMMIT=${FDOSDK_INFO[0]}
FDOSDK_LOCATION=${FDOSDK_INFO[1]}
GIR_ROOT=${FDOSDK_LOCATION}/files/share/gir-1.0

DEST_DIR=$(dirname ${0})


cp -a ${GIR_ROOT}/{Gio-2.0,GLib-2.0,GObject-2.0,IBus-1.0}.gir ${DEST_DIR}

git add ${DEST_DIR}/*.gir
git commit -m "gir-files: Get from FDO SDK ${FDOSDK_COMMIT}" ${DEST_DIR}/*.gir
